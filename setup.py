from setuptools import setup, find_packages

setup(
    name='channel_colocalization',
    version='0.1.0',
    description='Microscopy channel colocalization analysis',
    author='Ella Bahry',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'numpy>=1.26.4',
        'pandas>=2.2.1',
        'seaborn>=0.13.2',
        'matplotlib>=3.4.0',
    ],
)
