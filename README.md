# Channel Colocalization Analysis

This package provides tools for performing channel colocalization analysis on microscopy images. It enables users to apply various thresholding methods to images, calculate Manders' overlap coefficients, saves the results, and generate visualizations of the colocalization results.  

## Main Features

* Thresholding: Apply global thresholding methods (Otsu, Li, Triangle) to images for channel separation.
* Colocalization Analysis: Calculate Manders' overlap coefficients to quantify colocalization between two channels and seves the results.
* Visualization: Generate visual representations of thresholded images and colocalization analysis results.

## Installation

Clone this repounstall the package:  
`pip install .`  

## Example command:  

`python -m channel_colocalization --path /path/to/images --extension tif --c1_index 0 --c2_index 1`

## Usage

The tool requires command-line arguments to run:

`-p` or `--path` 
The path to the directory containing the images. The images should either be in sub-directories of experimental conditions or within a further sub-directory structure of the experimental runs.  

`-ext` or `--extension`:  
The file extension of the images (e.g., tif, png).  

`-c1i` or `--c1_index`, `-c2i` or `--c2_index`:  
Indexes specifying the channels to analyze (zero is the first channel).  

`-c1c` or `--c1_color`, `-c2c` or `--c2_color`:  
OPTIONAL Colors for the first and second channels (for visualization).  

`-tm` or `--threshold_method`:  
OPTIONAL Method to use for thresholding (choices: otsu, li, triangle).

## License

This project is licensed under the MIT License
