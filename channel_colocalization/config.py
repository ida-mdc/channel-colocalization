import argparse
from ida_tools import io_utils
from matplotlib.colors import LinearSegmentedColormap


def get_user_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('-p', '--path', required=True)
    parser.add_argument('-ext', '--extension', required=True)
    parser.add_argument('-c1i', '--c1_index', required=True, type=int)
    parser.add_argument('-c2i', '--c2_index', required=True, type=int)
    parser.add_argument('-c1c', '--c1_color', default='green')
    parser.add_argument('-c2c', '--c2_color', default='yellow')
    parser.add_argument('-tm', '--threshold_method', default='otsu', choices=['otsu', 'li', 'triangle'])

    arguments = parser.parse_args()

    return arguments


def get_channels_cmap():
    colors = ["black", ARGS.c1_color]
    c1_cmap = LinearSegmentedColormap.from_list(f"black_to_{ARGS.c1_color}", colors)
    colors = ["black", ARGS.c2_color]
    c2_cmap = LinearSegmentedColormap.from_list(f"black_to_{ARGS.c2_color}", colors)
    return c1_cmap, c2_cmap


THRESHOLD_METHODS = ['otsu', 'li', 'triangle']
ARGS = get_user_arguments()
RESULT_DIR = io_utils.create_result_dir(ARGS.path, 'colocalization_')
C1_CMAP, C2_CMAP = get_channels_cmap()
