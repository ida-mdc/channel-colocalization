from ida_tools import io_utils, image_operations
import os
from glob import glob
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from channel_colocalization.config import ARGS, THRESHOLD_METHODS, RESULT_DIR, C1_CMAP, C2_CMAP


def get_all_image_paths(path, ext):
    pattern = os.path.join(path, '**', f'*.{ext}')
    paths = glob(pattern, recursive=True)
    relative_paths = [os.path.relpath(p, path) for p in paths]
    return relative_paths


def paths_to_df(paths):
    data = []
    for p in paths:
        parts = p.split('/')
        if len(parts) == 2:
            condition, filename_with_ext = parts
            filename = os.path.splitext(filename_with_ext)[0]
            data.append((p, condition, None, filename))
        elif len(parts) == 3:
            condition, run, filename_with_ext = parts
            filename = os.path.splitext(filename_with_ext)[0]
            data.append((p, condition, run, filename))

    # Creating the dataframe to accommodate both structures
    return pd.DataFrame(data, columns=["path", "condition", "run", "filename"])


def save_thresholded_all_methods(df):
    conditions = df['condition'].unique().tolist()
    for c in conditions:
        c_paths = df[df["condition"] == c]["path"].tolist()
        c_paths = [os.path.join(ARGS.path, p) for p in c_paths]

        n_rows = len(c_paths) * 2

        fig = plt.figure(figsize=(15, 4 * n_rows), constrained_layout=True)
        fig.suptitle(f'{c}')

        sub_figs = fig.subfigures(nrows=n_rows, ncols=1)
        for row, sub_figs in enumerate(sub_figs):
            image_path = c_paths[row // 2]
            if row % 2 == 0:
                im = io_utils.read_image(image_path, ARGS.c1_index)
                cmap = C1_CMAP
            else:
                im = io_utils.read_image(image_path, ARGS.c2_index)
                cmap = C2_CMAP
            adjusted_image = image_operations.auto_contrast(im)

            sub_figs.suptitle(f'{os.path.basename(ARGS.path)}')
            axs = sub_figs.subplots(nrows=1, ncols=4)
            axs[0].imshow(adjusted_image, cmap=cmap)
            axs[0].axis('off')  # Turn off the axis
            axs[0].set_title('auto-contrast')

            for j in range(len(THRESHOLD_METHODS)):
                thr = image_operations.get_threshold(im, THRESHOLD_METHODS[j])
                bw = image_operations.make_binary(im, thr)

                axs[j + 1].imshow(bw, cmap=cmap)
                axs[j + 1].axis('off')  # Turn off the axis
                axs[j + 1].set_title(f'{THRESHOLD_METHODS[j]}')

        fig.savefig(os.path.join(RESULT_DIR, f'thresholded_images_{c}.png'))


def get_thresholded_gray_image(image, thr):
    return np.where(image < thr, 0, image)


def get_images_for_manders(image1, image2, thr1, thr2):
    image1_thresholded = get_thresholded_gray_image(image1, thr1)
    image2_thresholded = get_thresholded_gray_image(image2, thr2)

    overlap_mask = (image1_thresholded != 0) & (image2_thresholded != 0)

    return image1_thresholded, image2_thresholded, overlap_mask


def save_manders_images(image1_thresholded, image2_thresholded, overlap_mask, filename):
    io_utils.write_image(os.path.join(RESULT_DIR, f'{filename}_thresholded1.tif'), image1_thresholded)
    io_utils.write_image(os.path.join(RESULT_DIR, f'{filename}_thresholded2.tif'), image2_thresholded)
    io_utils.write_image(os.path.join(RESULT_DIR, f'{filename}_overlap_mask.tif'), overlap_mask.astype(np.uint8))


def manders_coefficients(image1, image2, thr1, thr2, filename):
    """
    Calculate Manders' coefficients for two images with given thresholds.

    Parameters:
    - image1, image2: 2D arrays of the same shape, representing two channels.

    Returns:
    - M1, M2: Manders' overlap coefficients.
    """

    image1_thresholded, image2_thresholded, overlap_mask = get_images_for_manders(image1, image2, thr1, thr2)

    save_manders_images(image1_thresholded, image2_thresholded, overlap_mask, filename)

    # Calculate sums for Manders' coefficients
    sum1 = np.sum(image1_thresholded)
    sum2 = np.sum(image2_thresholded)
    sum_overlap1 = np.sum(image1[overlap_mask])
    sum_overlap2 = np.sum(image2[overlap_mask])

    # Calculate M1 and M2
    m1 = sum_overlap1 / sum1 if sum1 > 0 else 0
    m2 = sum_overlap2 / sum2 if sum2 > 0 else 0

    return m1, m2


def plot_and_save_manders(df, is_show=False):
    ax = sns.boxplot(x='condition', y='M1', data=df, color='green')
    ax = sns.boxplot(x='condition', y='M2', data=df, color='yellow')
    ax.set_ylabel("Mander's Coefficient")

    # Create a legend
    legend_labels = ['M1', 'M2']
    legend_colors = [ARGS.c1_color, ARGS.c2_color]
    patches = [plt.Line2D([0], [0], marker='o', color='w', label=label,
                          markerfacecolor=color, markersize=10) for label, color in zip(legend_labels, legend_colors)]
    ax.legend(handles=patches)

    plt.savefig(os.path.join(RESULT_DIR, f'manders_boxplot.png'))
    if is_show:
        plt.show()
    plt.close()


def run_colocalization():
    paths = get_all_image_paths(ARGS.path, ARGS.extension)
    df = paths_to_df(paths)

    # save_thresholded_all_methods(df, threshold_methods)

    for i in df.index:
        image_path = os.path.join(ARGS.path, df.at[i, "path"])
        im1 = io_utils.read_image(image_path, 0)
        im2 = io_utils.read_image(image_path, 2)

        df.at[i, "c1_thr"] = image_operations.get_threshold(im1, ARGS.threshold_method)
        df.at[i, "c2_thr"] = image_operations.get_threshold(im2, ARGS.threshold_method)

        df.at[i, "M1"], df.at[i, "M2"] = manders_coefficients(im1, im2,
                                                              df.at[i, "c1_thr"], df.at[i, "c2_thr"],
                                                              df.at[i, "filename"])

    df.to_csv(os.path.join(RESULT_DIR, 'stats.csv'))
    plot_and_save_manders(df)
